<?php

namespace App\Http\Controllers;

use App\model\Brands;
use App\model\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
        $brands = Brands::getList();
        $company = Company::first();
        View::share('brands',$brands);
        View::share('company',$company);
    }

    protected function loginUser($id)
    {
        Auth::loginUsingId($id,true);
    }

    protected function generateCombo($array)
    {
        $hasil = [];
        $hasil[''] = 'Select';
        foreach($array as $key => $value)
        {
            $hasil[$value['id']] = $value['name'];
        }
        return $hasil;
    }

    protected function cekPermission($permission)
    {
        if(!Auth::user()->hasPermission($permission))
        {
            abort(405);
        }
    }
}
