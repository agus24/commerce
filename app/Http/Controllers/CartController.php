<?php

namespace App\Http\Controllers;

use App\UserCart;
use App\model\Discount;
use App\model\Order;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    public function addCart($product_id)
    {
        if(!Auth::guest())
        {
            $cart = new UserCart;
            $cart->insertOrUpdate(Auth::user()->id, $product_id);
        }
        else{
            return Redirect::back()->withErrors(['You need to login first before add an item']);
        }

        return redirect(url()->previous());
    }

    public function removeCart($id)
    {
        $cart = UserCart::find($id)->delete();
        return redirect(url()->previous());
    }

    public function checkout()
    {
        return view('user.checkout');
    }

    public function order()
    {
        $carts = (new UserCart)->getList(Auth::user()->id)->get();
        $customer_id = Auth::user()->id;
        $order = new Order;
        $invoice = $order->getLastOrderNumber();
        $detail = DB::table('orders_detail');
        $data_detail = array();
        $total = 0;
        $diskon = 0;
        $diskon_tmp = 0;
        $totalQty = 0;
        foreach($carts as $key=>$cart )
        {
            $data_detail[] = [
                "order_id" => $order->nextOrderId(),
                "product_id" => $cart->product_id,
                "price" => $cart->price,
                "qty" => $cart->qty
            ];
            $totalQty += $cart->qty;
            $percentage = Discount::getDiscount();
            $diskon_tmp += ($cart->price-($cart->price * ($percentage/100))) * $cart->qty;
            $total += ($cart->price * $cart->qty);
            $customer_id = $cart->user_id;
        }

        if($totalQty >= Discount::getQtyMin())
        {
            $diskon = $total * ($percentage/100);
        }

        $order->order_number = $invoice;
        $order->order_time = Carbon::now();
        $order->customer_id = Auth::user()->id;
        $order->total_order = $total;
        $order->discount = $diskon;
        $order->total_payment = 0;
        $order->save();

        $detail->insert($data_detail);

        $cart->clearCart(Auth::user()->id);
        return redirect('user/transaction');
    }

    public function modify($type,$id)
    {
        $cart = new UserCart;
        $cart->insertOrUpdate(Auth::user()->id, $id,$type);
        return redirect(url()->previous());
    }
}
