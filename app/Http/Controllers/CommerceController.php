<?php

namespace App\Http\Controllers;

use App\model\Product;
use Illuminate\Http\Request;

class CommerceController extends BaseController
{
    public function index()
    {
        $products = (new Product)->getList()->get()->groupby('brand_name');
        return view('home', compact('products'));
    }
}
