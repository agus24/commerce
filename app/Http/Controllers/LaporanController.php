<?php

namespace App\Http\Controllers;

use App\User;
use App\model\Order;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    //Show Mode
    //
    public function penjualanShow()
    {
        return view('admin.laporan.penjualan');
    }

    public function userShow()
    {
        $user = User::getNonAdmin()->get();
        return view('admin.laporan.plain',['tipe' => "user","data" => $user]);
    }

    public function penjualan(Request $request)
    {
        $this->validate($request,[
            "from" => "required|date",
            "to" => "required|date"
        ]);
        $order = (new Order)->between($request->from,$request->to);
        return view('admin.laporan.plain',['tipe' => "penjualan","data" => $order]);
    }

    public function user(Request $request)
    {
        //
    }
}
