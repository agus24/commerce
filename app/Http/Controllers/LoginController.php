<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class LoginController extends BaseController
{
    public function loginFront(Request $request)
    {
        $this->validate($request,[
            "username" => "required",
            "password" => "required"
        ]);

        $user = new User;

        $userCollection = $user->searchByEmail($request->input('username'))->where('is_admin',0)->first();
        if($userCollection != null)
        {
            if(Hash::check($request->input('password'), $userCollection->password))
            {
                $this->loginUser($userCollection->id);
                return redirect('/');
            }
        }

        abort(430);
    }

    //admin
    public function index()
    {
        if(!Auth::guest())
        {
            if(Auth::user()->is_admin == 1)
            {
                return redirect(Config::get('app.admin_url').'/home');
            }
            else
            {
                abort(405);
            }
        }

        return view('admin.login');
    }

    public function loginAdmin(Request $request)
    {
        $this->validate($request,[
            "username" => "required",
            "password" => "required"
        ]);

        $user = new User;

        $userCollection = $user->searchByEmail($request->input('username'))->where('is_admin',1)->first();
        if($userCollection != null)
        {
            if(Hash::check($request->input('password'), $userCollection->password))
            {
                $this->loginUser($userCollection->id);
                return redirect(Config::get('app.admin_url').'/home');
            }
        }

        return redirect(url()->previous());
    }

    public function register(Request $request)
    {
        $this->validate($request,[
            "user_email" => "required|unique:users,email",
            "user_password" => "required|same:confirmation_password",
            "name" => "required",
            "address" => "required",
            "city" => "required",
            "province" => "required",
            "zip_code" => "required",
            "phone_number" => "required",
        ]);

        $email = $request->input('user_email');
        $password = $request->input('user_password');
        $name = $request->input('name');
        $address = $request->input('address');
        $city = $request->input('city');
        $province = $request->input('province');
        $zip_code = $request->input('zip_code');
        $phone_number = $request->input('phone_number');

        $user = new User;

        $user->email = $email;
        $user->password = bcrypt($password);
        $user->name = $name;
        $user->address = $address;
        $user->city = $city;
        $user->province = $province;
        $user->zip_code = $zip_code;
        $user->phone_number = $phone_number;
        $user->is_admin = 0;
        $user->save();

        $this->loginUser($user->id);

        return redirect(url()->previous());
    }
}
