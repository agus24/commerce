<?php

namespace App\Http\Controllers;

use App\User;
use App\model\Order;
use App\model\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends \App\Http\Controllers\BaseController
{
    private $user;

    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User;
    }

    public function profile()
    {
        return view('user.profile');
    }

    public function edit()
    {
        return view('user.edit');
    }

    public function update(Request $request)
    {
        $user                   = $this->user->find(Auth::user()->id);
        $user->name             = $request->input('name');
        $user->address          = $request->input('address');
        $user->city             = $request->input('city');
        $user->province         = $request->input('province');
        $user->phone_number     = $request->input('phone');
        $user->save();

        return redirect('user/profile');
    }

    public function transaction()
    {
        $transactions = (new Order)->getAllUserOrder(Auth::user()->id)->get();
        // dd($transactions);
        return view('user.transaction',compact('transactions'));
    }

    public function transactionDetail($id)
    {
        $detail = OrderDetail::getFromOrderId($id);
        // dd($detail);
        return view('user.detail',compact("detail"));
    }
}
