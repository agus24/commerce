<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Requests;
use App\model\Brands;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Session;

class BrandsController extends BaseController
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->cekPermission('View Brands');
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $data = Brands::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $data = Brands::paginate($perPage);
        }
        // dd($brands);
        return view('admin.brands.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->cekPermission('Add Brands');
        return view('admin.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->cekPermission('Add Brands');
        $requestData = $request->all();

        Brands::create($requestData);

        Session::flash('flash_message', 'Brand added!');

        return redirect(Config::get('app.admin_url').'/brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brand = Brands::findOrFail($id);

        return view('admin.brands.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $this->cekPermission('Edit Brands');

        $brand = Brands::findOrFail($id);

        return view('admin.brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->cekPermission('Edit Brands');

        $requestData = $request->except(['_method','_token']);

        $brand = Brands::findOrFail($id);
        $brand->update($requestData);

        Session::flash('flash_message', 'Brand updated!');

        return redirect(Config::get('app.admin_url').'/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->cekPermission('Delete Brands');

        Brands::destroy($id);

        Session::flash('flash_message', 'Brand deleted!');

        return redirect(Config::get('app.admin_url').'/brands');
    }
}
