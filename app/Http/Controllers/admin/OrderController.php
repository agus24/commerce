<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\model\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        if(isset($_GET['payment']))
        {
            $payment_type = $_GET['payment'];
        }
        else
        {
            $payment_type = 'not_done';
        }
        $orders = (new Order)->getAllWithType($payment_type)->get();
        return view('admin.order.index',compact('orders'));
    }

    public function show($id)
    {
        $orders = (new Order)->findWithDetail($id);
        return view('admin.order.show',compact('orders'));
    }

    public function payment($id)
    {
        return view('admin.order.payment',['payment' => $id]);
    }

    public function addPayment(Request $request, $id)
    {
        $order = new Order;
        $order->addPayment($id,$request->input('payment'));
        return redirect(Config::get('app.admin_url')."/order");
    }
}
