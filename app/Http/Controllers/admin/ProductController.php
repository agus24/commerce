<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as BaseController;
use App\model\Brands;
use App\model\Product;
use App\model\Specification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ProductController extends BaseController
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->cekPermission('View Products');
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $products = (new Product)->getList()
                        ->where('products.name','LIKE',"%$keyword%")
                        ->orwhere('specifications.name','LIKE',"%$keyword%")
                        ->orwhere('brands.name','LIKE',"%$keyword%")
                        ->orwhere('description','LIKE',"%$keyword%")
                        ->orwhere('price','LIKE',"%$keyword%")
                        ->orderby('id','desc')
                        ->paginate($perPage);
        } else {
            $products = (new Product)->getList()
                    ->orderby('id','desc')
                    ->paginate($perPage);
        }
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->cekPermission('Add Products');

        $cmb = $this->cmb();
        return view('admin.product.create',compact('cmb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->cekPermission('Add Products');

        // dd($request);
        $this->validate($request,[
            "name" => "required",
            "brand_id" => "required|integer",
            "specification_id" => "required|integer",
            "description" => "required",
            "price" => "required|numeric",
            "picture" => "required|file",
        ]);
        $picture = $request->picture->store('image/product','public');
        $picture = $request->picture->hashName();
        $product = new Product;

        $product->name = $request->input('name');
        $product->brand_id = $request->input('brand_id');
        $product->specification_id = $request->input('specification_id');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->picture = $picture;
        $product->featured = !empty($request->input('featured'));
        $product->status = !empty($request->input('status'));
        $product->save();

        return redirect(Config::get('app.admin_url')."/products");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->cekPermission('Edit Products');

        $products = Product::findOrFail($id);
        $cmb = $this->cmb();

        return view('admin.product.edit',[
                "products" => $products,
                "cmb" => $cmb
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->cekPermission('Edit Products');

        $this->validate($request,[
            "name" => "required",
            "brand_id" => "required|integer",
            "specification_id" => "required|integer",
            "description" => "required",
            "price" => "required|numeric",
        ]);

        $product = Product::findOrFail($id);

        if($request->picture != null)
        {
            $picture = $request->picture->store('image/product','public');
            $picture = $request->picture->hashName();
        }
        else{
            $picture = $product->picture;
        }

        $product->name = $request->input('name');
        $product->brand_id = $request->input('brand_id');
        $product->specification_id = $request->input('specification_id');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->picture = $picture;
        $product->featured = !empty($request->input('featured'));
        $product->status = !empty($request->input('status'));
        $product->save();

        return redirect(Config::get('app.admin_url')."/products");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->cekPermission('Delete Products');

        Product::destroy($id);

        Session::flash('flash_message', 'Product deleted!');

        return redirect(Config::get('app.admin_url').'/products');
    }

    private function cmb()
    {
        return [
            'brand' => $this->generateCombo((new Brands)->select('id','name')->get()->toArray()),
            'spec'  => $this->generateCombo((new Specification)->select('id','name')->get()->toArray()),
        ];
    }
}
