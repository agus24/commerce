<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as BaseController;

use App\User;
use App\model\Company;
use App\model\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Models\Permission;

class SettingController extends BaseController
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $this->cekPermission('View User');
        $users = User::orderby('is_admin','desc')->get();
        return view('admin.settings.index', compact('users'));
    }

    public function create()
    {
        $this->cekPermission('Add User');

        return view('admin.settings.create');
    }

    public function insert(Request $request)
    {
        $this->cekPermission('Add User');

        $this->validate($request,[
            "email"         => "required|email|unique:users",
            "password"      => "required|confirmed",
            "name"          => "required",
            "address"       => "required",
            "city"          => "nullable",
            "province"      => "nullable",
            "zip_code"      => "nullable|numeric",
            "phone_number"  => "nullable",
        ]);

        $user = new User;

        $user->name         = $request->input('name');
        $user->email        = $request->input('email');
        $user->password     = bcrypt($request->input('password'));
        $user->is_admin     = $request->input('is_admin');
        $user->address      = $request->input('address');
        $user->city         = $request->input('city');
        $user->province     = $request->input('province');
        $user->zip_code     = $request->input('zip_code');
        $user->phone_number = $request->input('phone_number');
        $user->is_admin     = 1;
        $user->save();

        return redirect(url()->previous());
    }

    public function edit($id)
    {
        $this->cekPermission('Edit User');

        return view('admin.settings.edit', ["user" => User::find($id)]);
    }


    public function update(Request $request,$id)
    {
        $this->cekPermission('Edit User');

        $this->validate($request,[
            "name"          => "required",
            "email"         => "required|email",
            "password"      => "required",
            "address"       => "required",
            "city"          => "required",
            "province"      => "required",
            "zip_code"      => "required|numeric",
            "phone_number"  => "required",
        ]);

        $user = User::find($id);
        $user->name         = $request->input('name');
        $user->email        = $request->input('email');
        $user->password     = bcrypt($request->input('password'));
        $user->is_admin     = $request->input('is_admin');
        $user->address      = $request->input('address');
        $user->city         = $request->input('city');
        $user->province     = $request->input('province');
        $user->zip_code     = $request->input('zip_code');
        $user->phone_number = $request->input('phone_number');
        $user->is_admin     = 1;
        $user->save();

        return redirect(url()->previous());
    }

    public function delete($id)
    {
        $this->cekPermission('Delete User');
        $user = User::find($id);
        $user->delete();
        return redirect(url()->previous());
    }

    public function showPermission($id)
    {
        $this->cekPermission('Permission User');
        $permissions = Permission::all();
        $user_permission = User::find($id)->permissions;
        return view('admin.settings.showPermission',['permissions'=>$permissions,"user_permission"=>$user_permission,"id"=>$id]);
    }

    public function updatePermission(Request $request, $id)
    {
        $this->cekPermission('Permission User');
        $prm = $request->input('permission') == null ? [] : $request->input('permission');
        $user = User::find($id);
        $permissions = Permission::select('name')->get()
                    ->map(function($item,$key) {
                            return $item->name;
                        })->toArray();
        $user->revokePermissionTo($permissions);
        $user->givePermissionTo($prm);

        return redirect(Config::get('app.admin_url')."/user");

    }

    public function company()
    {
        $this->cekPermission('View Company');
        $company = Company::first();
        return view('admin.settings.company',compact('company'));
    }

    public function companyUpdate(Request $request)
    {
        $this->cekPermission('Edit Company');
        $this->validate($request,[
            "company_name" => "required",
            "address" => "required",
            "city" => "required",
            "province" => "required",
            "country" => "required",
            "phone" => "required",
            "email" => "required",
        ]);

        $company = Company::find(1);

        $company->company_name = $request->input('company_name');
        $company->address = $request->input('address');
        $company->city = $request->input('city');
        $company->province = $request->input('province');
        $company->country = $request->input('country');
        $company->phone = $request->input('phone');
        $company->email = $request->input('email');

        $company->save();

        return redirect(url()->previous());
    }

    public function discount()
    {
        $this->cekPermission('View Discount');
        $discount = Discount::find(1);
        return view('admin.settings.discount', compact('discount'));
    }

    public function discountUpdate(Request $request)
    {
        $this->cekPermission('Edit Company');
        $this->validate($request,[
            "qty" => "required|numeric",
            "percent" => "required|numeric",
        ]);

        $discount = Discount::find(1);

        $discount->qty = $request->input('qty');
        $discount->percent = $request->input('percent');
        $discount->save();

        return redirect(url()->previous());
    }
}
