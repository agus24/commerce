<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController as BaseController;

use App\model\Specification;
use Illuminate\Http\Request;
use Session;

class SpecificationsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        $this->middleware('admin');

    }

    public function index(Request $request)
    {
        $this->cekPermission('View Specification');
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $specs = Specification::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $specs = Specification::paginate($perPage);
        }

        return view('admin.specs.index', compact('specs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->cekPermission('Add Specification');

        return view('admin.specs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->cekPermission('Add Specification');

        $requestData = $request->all();

        Specification::create($requestData);

        Session::flash('flash_message', 'spec added!');

        return redirect('admin-page/specs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $spec = Specification::findOrFail($id);

        return view('admin.specs.show', compact('spec'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $this->cekPermission('Edit Specification');

        $spec = Specification::findOrFail($id);

        return view('admin.specs.edit', compact('spec'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->cekPermission('Edit Specification');

        $requestData = $request->all();

        $spec = Specification::findOrFail($id);
        $spec->update($requestData);

        Session::flash('flash_message', 'spec updated!');

        return redirect('admin-page/specs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->cekPermission('Delete Specification');

        Specification::destroy($id);

        Session::flash('flash_message', 'spec deleted!');

        return redirect('admin-page/specs');
    }
}
