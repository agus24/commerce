<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest())
        {
            return redirect(Config::get('app.admin_url'));
        }
        if(Auth::user()->is_admin == 0)
        {
            return redirect(Config::get('app.admin_url'));
        }

        return $next($request);
    }
}
