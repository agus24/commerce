<?php

namespace App\Http\Middleware;

use Closure;

class Authorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
        public function handle($request, Closure $next,  $permission = null, ...$role)
        {
        // dd($role);
            if (Auth::guest()) {
                return redirect(Config::get('app.admin_url')."/login");
            }

            if (! $request->user()->hasAnyRole($role)) {
               abort(403);
            }

            if($permission != null)
            {
                if (! $request->user()->can($permission)) {
                   abort(403);
                }
            }

            return $next($request);
        }
}
