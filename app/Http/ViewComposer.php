<?php

namespace App\Http;

use App\UserCart;
use App\model\Brands;
use App\model\Company;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ViewComposer
{
    public function compose(View $view)
    {
        $view->with('company', Company::first());
        $view->with('brands', Brands::getList());
        if(!Auth::guest())
        {
            $user_id = Auth::user()->id;
        }
        else
        {
            $user_id = 0;
        }
        $cart = (new UserCart)->getList($user_id)->get();
        $view->with('cart', $cart);
    }
}
