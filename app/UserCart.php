<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    protected $table = "user_cart";
    protected $primaryKey = 'id';

    public function getList($user_id)
    {
        return $this->join('products', 'products.id','user_cart.product_id')
                    ->select("user_cart.*",'products.name as product_name','products.price as price')
                    ->where('user_id', $user_id);
    }

    public function insertOrUpdate($user_id,$product_id,$type='plus')
    {
        $data = $this->where('user_id', $user_id)->where('product_id', $product_id);
        $count = $data->get();
        if($count->count() > 0)
        {
            if($type == 'plus')
            {
                $data->update(['qty' => $count[0]->qty+1]);
            }
            else{
                $data->update(['qty' => $count[0]->qty-1]);
            }
        }
        elseif($count->count() == 0)
        {
            $this->insert(["user_id" => $user_id, "product_id" => $product_id, "qty" => 1]);
        }
    }

    public function removeCart($user_id, $product_id)
    {
        $this->where('user_id', $user_id)->where('product_id', $product_id)->delete();
    }

    public function clearCart($user_id)
    {
        $this->where('user_id', $user_id)->delete();
    }
}
