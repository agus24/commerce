<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brands extends Model
{
    use SoftDeletes;
    protected $table = "brands";
    protected $primaryKey = "id";
    protected $fillable = ['name'];

    public function scopegetList()
    {
        return $this->get();
    }
}
