<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'discounts';

    public function scopegetQtyMin()
    {
        return $this->first()->qty;
    }

    public function scopegetDiscount()
    {
        return $this->first()->percent;
    }
}
