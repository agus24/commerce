<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';

    public function getAllUserOrder($user_id)
    {
        return $this->where('customer_id',$user_id)->orderby('order_time','desc');
    }

    public function getDetailFromId($id)
    {
        return DB::table('orders_detail')->where('order_id',$id)->get();
    }

    public function getLastOrderNumber()
    {
        if($this->orderBy('id','desc')->first() == null)
        {
            return "INV/"."0001";
        }
        else
        {
            return "INV/".(explode('/', $this->orderBy('id','desc')->first()->order_number)[1]+1);
        }
    }

    public function nextOrderId()
    {
        if($this->orderBy('id','desc')->first() == null)
        {
            return 1;
        }
        else
        {
            return $this->orderBy('id','desc')->first()->id+1;
        }
        // return ->id;
    }

    public function getAllWithType($type)
    {
        $data = $this->join('users','users.id','orders.customer_id')->select('orders.*','users.name as customer_name');
        if($type == "done")
        {
            return $data->where(DB::raw('total_order-discount'),'=',DB::raw('total_payment'));
        }
        elseif($type == 'not_done')
        {
            return $data->where(DB::raw('total_payment'),'<>',DB::raw('total_order-discount'));
        }
        elseif($type == 'all'){
            return $data;
        }
    }

    public function findWithDetail($id)
    {
        return [
            "header" => $this->join('users','users.id','orders.customer_id')->select('orders.*','users.name as customer_name')->where('orders.id',$id)->get()[0],
            "detail" => DB::table('orders_detail')->where('order_id',$id)->join('products','products.id','orders_detail.product_id')->select('orders_detail.*','products.name as product_name')->get()
        ];
    }

    public function addPayment($id,$payment)
    {
        $data = $this->where('id', $id)->get()[0];
        $this
            ->where('id',$id)
            ->update([
                "total_payment" => $data->total_payment + $payment
            ]);
    }

    public function between($from,$to)
    {
        return $this->join('users','users.id','orders.customer_id')
                ->select('orders.*','users.name as customer_name')
                ->wherebetween('order_time',[$from,$to])
                ->get();
    }
}
