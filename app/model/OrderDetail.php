<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'orders_detail';

    public function scopegetFromOrderId($query,$order_id)
    {
        return $query->where('order_id',$order_id)
                    ->join('products','products.id','orders_detail.product_id')
                    ->select('orders_detail.*','products.name')
                    ->get();
    }
}
