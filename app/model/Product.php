<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function getList()
    {
        //SELECT * FROM `products` inner join brands on products.brand_id = brands.id inner join specifications on products.specification_id = specifications.id
        return $this->join('brands','products.brand_id','brands.id')
                    ->join('specifications','specifications.id','products.specification_id')
                    ->select('products.*','brands.name as brand_name','specifications.name as spec_name')
                    ->orderby('status','desc')
                    ->orderby('name','asc')
                    // ->where('products.status',1)
                    ;
    }
}
