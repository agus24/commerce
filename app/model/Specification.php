<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specification extends Model
{
    use SoftDeletes;

    protected $primaryKey = "id";
    protected $table = "specifications";
    protected $fillable = ['name'];
}
