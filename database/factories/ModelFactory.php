<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'is_admin' => $faker->numberBetween(1, 0),
        "address" => $faker->address,
        "city" => $faker->cityPrefix,
        "province" => $faker->state,
        "zip_code" => $faker->postcode,
        "phone_number" => $faker->phoneNumber
    ];
});

// $factory->define(App\model\Category::class, function (Faker\Generator $faker){
//     return [
//         "name" => $faker->word
//     ];
// });

$factory->define(App\model\Product::class, function (Faker\Generator $faker){
    $pic = [
        "florence1-san-giovanni-180x200n.jpg",
        "florence2-san-pietro-hanya-kasu.jpg",
        "KingKoil1___Kasur_51973a6639b61.jpg",
        "Kingkoil2-Anna-new.JPG",
        "Serta1solitaire13.jpg",
        "Serta2___Kasur.jpg"
    ];
    return [
        "name" => $faker->word,
        "picture" => $pic[$faker->numberBetween(0,5)],
        "description" => $faker->sentence,
        "brand_id" => $faker->numberBetween(1, 3),
        "specification_id" => $faker->numberBetween(1, 4),
        "price" => $faker->numberBetween(500000,100000),
        "featured" => $faker->numberBetween(0,1),
        "status" => $faker->numberBetween(0,1),
    ];
});


