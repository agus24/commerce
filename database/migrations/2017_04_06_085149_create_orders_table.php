<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->dateTime('order_time');
            $table->integer('customer_id');
            $table->integer('total_order');
            $table->integer('discount');
            $table->integer('total_payment');
            $table->timestamps();
        });

        Schema::create('orders_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('price');
            $table->integer('qty');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_detail', function (Blueprint $table) {
            $table->dropForeign('orders_detail_order_id_foreign');
            $table->dropForeign('orders_detail_product_id_foreign');
        });

        Schema::dropIfExists('orders_detail');

        Schema::dropIfExists('orders');
    }
}
