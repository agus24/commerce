<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ["name" => "Top Seller"],
            ["name" => "Other"],
        ]);
        // factory(App\model\Category::class, 20)->create();
    }
}
