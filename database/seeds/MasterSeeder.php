<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "email" => "admin@admin.com",
                "password" => bcrypt('admin'),
                "name" => 'admin',
                "is_admin" => 1,
                "address" => "none",
                "city" => "none",
                "province" => "none",
                "zip_code" => "none",
                "phone_number" => "none"
            ],
            [
                "email" => "agusx244@gmail.com",
                "password" => bcrypt('rahasia'),
                "name" => 'Gustiawan Ouwawi',
                "is_admin" => 1,
                "address" => "Jl. Prepedan No. 32",
                "city" => "Jakarta Barat",
                "province" => "DKI Jakarta",
                "zip_code" => "11810",
                "phone_number" => "081221800812"
            ]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        for($i = 1; $i <= 26; $i++)
        {
            DB::table('user_has_permissions')->insert([
                [
                    "user_id" => 1,
                    "permission_id" => $i
                ],[
                    "user_id" => 2,
                    "permission_id" => $i
                ]
            ]);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('specifications')
            ->insert([
                [
                    "name" => 120
                ],[
                    "name" => 160
                ],[
                    "name" => 180
                ],[
                    "name" => 200
                ]
            ]);
        DB::table('brands')->insert([
            [
                "name" => "kingkoil"
            ],[
                "name" => "serta"
            ],[
                "name" => "florence"
            ]
        ]);

        DB::table('companies')->insert([
            [
                "company_name" => "PT. Duta Abadi Primantara",
                "address" => "Jl. Buntu No. 99",
                "city" => "Jakarta Barat",
                "province" => "DKI Jakarta",
                "country" => "Indonesia",
                "phone" => "123456",
                "email" => "asd@asd.com"
            ]
        ]);

        DB::table('discounts')->insert([
            ["qty"=>10,"percent"=>5]
        ]);

        DB::table('permissions')->insert([
            ["name" => "View Brands"],
            ["name" => "Add Brands"],
            ["name" => "Edit Brands"],
            ["name" => "Delete Brands"],

            ["name" => "View Specification"],
            ["name" => "Add Specification"],
            ["name" => "Edit Specification"],
            ["name" => "Delete Specification"],

            ["name" => "View Products"],
            ["name" => "Add Products"],
            ["name" => "Edit Products"],
            ["name" => "Delete Products"],

            ["name" => "View Order"],
            ["name" => "Add Order"],
            ["name" => "Edit Order"],
            ["name" => "Delete Order"],

            ["name" => "View User"],
            ["name" => "Add User"],
            ["name" => "Edit User"],
            ["name" => "Delete User"],
            ["name" => "Permission User"],

            ["name" => "View Company"],
            ["name" => "Edit Company"],

            ["name" => "View Discount"],
            ["name" => "Edit Discount"],

            ["name" => "View Report"],

        ]);
    }
}
