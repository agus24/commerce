<?php

use App\model\Product;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $detail = array();
        // factory(App\model\Order::class, 50)->create();
        for ($i = 0; $i < 50; $i++) {
            $total_price = 0;
            $randomDetail = 5;
            $product_id = $this->getRandomNumber($randomDetail);
            $order_id = $i+1;
            $detail = array();
            for($j = 0 ; $j < $randomDetail; $j++)
            {
                $detail[] =
                [
                    "order_id" => $order_id,
                    "product_id" => $product_id[$j],
                    "price" => $price = Product::find($product_id[$j])->price,
                    "qty" => $qty = rand(1,10)
                ];
                $total_price += $price*$qty;
            }

            DB::table('orders')->insert([
                    "order_number" => "INV/".mt_rand(1000,9999),
                    "order_time" => date("Y-m-d H:i:s",mt_rand(1262055681,1262055681)),
                    "customer_id" => rand(1,10),
                    "total_order" => $total_price,
                    "discount" => $disc = $total_price * 10/100,
                    "total_payment" => $total_price - $disc
            ]);
            DB::table('orders_detail')->insert($detail);
        }
    }

    private function checkNum($collection,$number)
    {
        if(!$collection->search($number))
        {
            return $number;
        }
        else{
            return $this->checkNum($collection,rand(1,10));
        }
    }

    private function getRandomNumber($totalNumber,$maxNumber = 10)
    {
        $numbers = collect([]);
        for ($i=0; $i < $totalNumber; $i++)
        {
            $numbers->push($this->checkNum($numbers,rand(1,10)));
        }

        return $numbers;
    }
}
