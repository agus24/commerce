@if($tipe == "penjualan")
    <center><h1>Laporan Penjualan</h1></center>

    <table width="100%" border=1>
    <thead>
        <th>No.</th>
        <th>Tanggal / Waktu</th>
        <th>Customer</th>
        <th>Total Order</th>
        <th>Discount</th>
        <th>Total Payment</th>
        <th>Status</th>
    </thead>
    <tbody>
    @foreach($data as $key => $value)
        <tr>
            <td>{{ $key+1 }}.</td>
            <td>{{ $value->order_time}}</td>
            <td>{{ $value->customer_name}}</td>
            <td align="right">{{ number_format($value->total_order) }}</td>
            <td align="right">{{ number_format($value->discount) }}</td>
            <td align="right">{{ number_format($value->total_payment) }}</td>
            <td style="background-color:{{ ($value->total_order - $value->total_payment) == 0? "#9af99a" : "#f78181" }}">
                {{ ($value->total_order - $value->total_payment) == 0? "Lunas" : "Belum Lunas" }}
            </td>
        </tr>
    @endforeach
    </tbody>
    </table>
@elseif($tipe == 'user')
<center><h1>User Report</h1></center>

    <table width="100%" border=1>
    <thead>
        <th>No.</th>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>City</th>
        <th>Province</th>
        <th>Zip Code</th>
        <th>Phone Number</th>
    </thead>
    <tbody>
    @foreach($data as $key => $value)
        <tr>
            <td>{{ $key+1 }}.</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->address }}</td>
            <td>{{ $value->city }}</td>
            <td>{{ $value->province }}</td>
            <td>{{ $value->zip_code }}</td>
            <td>{{ $value->phone_number }}</td>
        </tr>
    @endforeach
    </tbody>
    </table>


@endif
