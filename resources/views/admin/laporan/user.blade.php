@extends('layouts.admin.app')

@section('content')
<br>
<div id="base">
<div id="content">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head"><header>Laporan Penjualan</header></div>
                    <div class="card-body">

                        {!! Form::open(['method' => 'POST', 'url' => '/admin-page/laporan/user', 'class' => 'navbar-form', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <label>From</label>
                            <input type="date" class="form-control" name="from" value="{{ date('Y-m-01') }}">
                        </div>
                        <div class="input-group">
                        <label>To</label>
                            <input type="date" class="form-control" name="to" value="{{ date('Y-m-d') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
</div>
@endsection
