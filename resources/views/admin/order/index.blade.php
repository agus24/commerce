@extends('layouts.admin.app')
@section('content')
<div id="base">
<div id="content">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head"><header>Order</header></div>
                    <div class="card-body">
                    <label>Payment</label>
                    <select class="form-control" onchange="window.location.href = '?payment='+this.value">
                        <option {{ (isset($_GET['payment']) && $_GET['payment'] == "not_done"? "selected" : "" ) }}  value="not_done">Not Done</option>
                        <option {{ (isset($_GET['payment']) && $_GET['payment'] == "done"? "selected" : "" ) }} value="done">Done</option>
                        <option {{ (isset($_GET['payment']) && $_GET['payment'] == "all"? "selected" : "" ) }}  value="all">All</option>
                    </select>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-bordered table-stripped">
                                <thead>
                                    <tr>
                                        <th width="5%"><center>No.</center></th>
                                        <th width="15%"><center>Order Number</center></th>
                                        <th width="12%"><center>Date</center></th>
                                        <th width="12%"><center>Customer</center></th>
                                        <th width="5%"><center>Total Order</center></th>
                                        <th width="12%"><center>Discount</center></th>
                                        <th width="12%"><center>Payment</center></th>
                                        <th width="12%"><center>Remaining</center></th>
                                        <th width="20%"><center>Actions</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $order)
                                <tr>
                                    <td align="center">{{ $key+1 }}.</td>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ $order->order_time }}</td>
                                    <td>{{ $order->customer_name }}</td>
                                    <td align="right">{{ number_format($order->total_order) }}</td>
                                    <td align="right">{{ number_format($order->discount) }}</td>
                                    <td align="right">{{ number_format($order->total_payment) }}</td>
                                    <td align="right">{{ number_format($order->total_order - $order->discount - $order->total_payment ) }}</td>
                                    <td align="center">
                                        <a href="{{ url(Config::get('app.admin_url').'/order/payment/'.$order->id) }}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Add Payment</a>
                                        <a href="{{ url(Config::get('app.admin_url').'/order/'.$order->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Detail</a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => [Config::get('app.admin_url').'/products', $order->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete spec',
                                                    !Auth::user()->hasPermission('Delete Products')? "disabled" : "",
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{-- <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
@endsection
