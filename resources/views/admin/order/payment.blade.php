@extends('layouts.admin.app')
@section('content')
<div id="base">
<div id="content">
<section>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-head"><header>Order</header></div>
            <div class="card-body">
            <div class="col-md-6">
                <form method="POST" action="{{ url(Config::get('app.admin_url').'/order/payment/'.$payment) }}">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="row">
                        <div class="col-md-3">
                            <label>Payment</label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="payment">
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>
</div>
@endsection
