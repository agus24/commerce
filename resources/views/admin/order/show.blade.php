@extends('layouts.admin.app')

@section('content')
<div id="base">
<div id="content">
<section>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-head"><header>Order {{ $orders['header']->order_number }}</header></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $orders['header']->id }}</td>
                                </tr>
                                <tr><th> Date </th><td> {{ $orders['header']->order_time }} </td></tr>
                                <tr><th> Invoice </th><td> {{ $orders['header']->order_number }} </td></tr>
                                <tr><th> Customer </th><td> {{ $orders['header']->customer_name }} </td></tr>
                                <tr><th> Total </th><td> {{ number_format($orders['header']->total_order) }} </td></tr>
                                <tr><th> Discount </th><td> {{ number_format($orders['header']->discount) }} </td></tr>
                                <tr><th> Payment </th><td> {{ number_format($orders['header']->total_payment) }} </td></tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                        @foreach($orders['detail'] as $key => $detail)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $detail->product_name}}</td>
                            <td>{{ number_format($detail->qty) }}</td>
                            <td>{{ number_format($detail->price) }}</td>
                            <td>{{ number_format($detail->price * $detail->qty) }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
</div>
@endsection
