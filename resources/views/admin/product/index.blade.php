@extends('layouts.admin.app')

@section('content')
<br>
<div id="base">
<div id="content">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head"><header>Product</header></div>
                    <div class="card-body">
                        @can('Add Products')
                            <a href="{{ url(Config::get('app.admin_url').'/products/create') }}" class="btn btn-success btn-sm" title="Add New spec">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        @endcan

                        {!! Form::open(['method' => 'GET', 'url' => Config::get('app.admin_url').'/products', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th width="15%">Picture</th>
                                        <th width="12%">Name</th>
                                        <th width="12%">Brand</th>
                                        <th width="5%">Specification</th>
                                        <th width="12%">Description</th>
                                        <th width="12%">Price</th>
                                        <th width="12%">Status</th>
                                        <th width="20%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key=>$item)
                                    <tr>
                                        @if(isset($_GET['page']))
                                        <td>{{ ($_GET['page'] >1) ?(($_GET['page']-1)*25)+$key+1 : $key+1 }}.</td>
                                        @else
                                        <td>{{$key+1}}.</td>
                                        @endif
                                        <td><img src="{{ asset('Storage/image/product/'.$item->picture) }}" width="50%"></td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->brand_name }}</td>
                                        <td align="right">{{ $item->spec_name }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td align="right">Rp. {{ number_format($item->price) }}</td>
                                        <td align="center"><span class="btn-{{ $item->status == 1 ? "success" : "danger" }} btn-xs"><b>{{ $item->status == 1 ? "ACTIVE" : "INACTIVE" }}</b></span></td>
                                        <td>
                                        @can('Edit Products')
                                            <a href="{{ url(Config::get('app.admin_url').'/products/' . $item->id . '/edit') }}" title="Edit Prodcut"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        @endcan
                                        @can('Delete Products')
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => [Config::get('app.admin_url').'/products', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete spec',
                                                        !Auth::user()->hasPermission('Delete Products')? "disabled" : "",
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
</div>
@endsection
