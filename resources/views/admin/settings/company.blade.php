@extends('layouts.admin.app')

@section('content')
<div id="base">
    <div id="content">
        <section>
            <div class="section-body">
                <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-primary">Company</h1>
                </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" method="post" action="{{ url(Config::get('app.admin_url').'/company') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="company_name" value="{{ $company->company_name }}">
                                        <label for="regular1">Company Name</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="address" value="{{ $company->address }}">
                                        <label for="regular1">Address</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="city" value="{{ $company->city }}">
                                        <label for="regular1">City</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="province" value="{{ $company->province }}">
                                        <label for="regular1">Province</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="country" value="{{ $company->country }}">
                                        <label for="regular1">Country</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" value="{{ $company->phone }}">
                                        <label for="regular1">Phone</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" value="{{ $company->email }}">
                                        <label for="regular1">Email</label>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary">
                                    </div>
                                </form>
                            </div><!--end .card-body -->
                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>
@endsection
