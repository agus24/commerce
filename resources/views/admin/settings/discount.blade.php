@extends('layouts.admin.app')

@section('content')
<div id="base">
    <div id="content">
        <section>
            <div class="section-body">
                <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-primary">Discount</h1>
                </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" method="post" action="{{ url(Config::get('app.admin_url').'/discount') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="qty" value="{{ $discount->qty }}">
                                        <label for="regular1">Qty</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="percent" value="{{ $discount->percent }}">
                                        <label for="regular1">Discount</label>
                                        <p class="help-block" style="color:red">It Means Each Number of Qty will give you a discount <i><b>n</b></i> percent</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary">
                                    </div>
                                </form>
                            </div><!--end .card-body -->
                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>
@endsection
