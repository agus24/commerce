@extends('layouts.admin.app')

@section('content')
<div id="base">
    <div id="content">
        <section>
            <div class="section-body">
                <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-primary">User</h1>
                </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form" action="{{ url(Config::get('app.admin_url')."/user")."/".$user->id }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('patch') }}
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="email" readonly value="{{ $user->email }}">
                                    <label for="regular1">Email</label>
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                    <input type="password" class="form-control" name="password" value="">
                                    <label for="regular1">password</label>
                                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                                    <label for="regular1">Name</label>
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                                    <label for="regular1">Address</label>
                                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="city" value="{{ $user->city }}">
                                    <label for="regular1">City</label>
                                    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('province') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="province" value="{{ $user->province }}">
                                    <label for="regular1">Province</label>
                                    {!! $errors->first('province', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="zip_code" value="{{ $user->zip_code }}">
                                    <label for="regular1">Zip Code</label>
                                    {!! $errors->first('zip_code', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
                                    <input type="text" class="form-control" name="phone_number" value="{{ $user->phone_number }}">
                                    <label for="regular1">Phone Number</label>
                                    {!! $errors->first('zip_code', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary">
                                    <a href="{{ url()->previous() }}" class="btn btn-danger">Back</a>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>
@endsection
