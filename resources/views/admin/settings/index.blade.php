@extends('layouts.admin.app')

@section('content')
<div id="base">
    <div id="content">
        <section>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                            @can('Add User')
                                <a href="{{ url(Config::get('app.admin_url').'/user/create') }}" class="btn btn-success btn-sm" title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>
                            @endcan
                                <table class="table no-margin">
                                    <thead>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Province</th>
                                        <th>Role</th>
                                        <th>Permission</th>
                                        <th width="8%">#</th>
                                    </thead>
                                    <Tbody>
                                    @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{ $key+1 }}.</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->address }}</td>
                                        <td>{{ $user->city }}</td>
                                        <td>{{ $user->province }}</td>
                                        <td>{!! $user->is_admin == 1 ? "<span style='color:#4caf50'>Admin</span>" : "<span style='color:#ff9800'>Member</span>" !!}</td>
                                        <td>
                                            @if($user->is_admin)
                                            <a href="{{ url(Config::get('app.admin_url').'/user/').'/'.$user->id."/permission" }}"
                                                class="btn btn-primary btn-sm"
                                                {{ !Auth::user()->hasPermission('Permission User')? "disabled" : "" }}>
                                                Change Permission
                                            </a>
                                            @else
                                            <button href="#" class="btn btn-primary btn-sm" disabled>Change Permission</button>
                                            @endif
                                        </td>
                                        <td>
                                        @can('Edit User')
                                            <a href="{{ url(Config::get('app.admin_url').'/user/').'/'.$user->id."/edit" }}" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('Delete User')
                                            <a href="{{ url(Config::get('app.admin_url').'/user/').'/'.$user->id."/delete" }}" class="btn btn-danger btn-sm del"><i class="fa fa-trash"></i></a>
                                        @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                    </Tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

    </div>
</div>
@endsection

@section('script')
<script>
$('.del').each(function() {
    $(this).on('click', function() {
        return confirm('Are You Sure?');
    });
});
</script>
@endsection
