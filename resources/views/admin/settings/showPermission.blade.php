@extends('layouts.admin.app')

@section('content')
<div id="base">
<div id="content">
<section>
<div class="section-body">
<div class="row">
<div class="col-lg-12">
    <h1 class="text-primary">User</h1>
</div>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form action="{{ url(Config::get('app.admin_url')."/user/".$id."/permission") }}" method="POST">
        {{ csrf_field() }}
        <button class="btn btn-primary">Submit</button>
        <a href="{{ url()->previous() }}"><input type="button" class="btn btn-danger" value="Back"></a>
        <table class="table table-bordered table-stripped">
        <tr>
            <td><b>Permission Name</b></td>
            <td><input type="checkbox" id="checkAll"></td>
        </tr>
        @foreach($permissions as $permission)
        <tr>
            <td>{{ $permission->name }}</td>
            <td><input type="checkbox" class="permission" name="permission[]" value="{{ $permission->name }}" {{ count($user_permission->where('name',$permission->name)) > 0 ? "checked" : "" }} ></td>
        </tr>
        @endforeach
        </table>
        </form>
        </div>
    </div>
</div>
</div>
</div>
</section>
</div>
@endsection

@section('script')
<script>
$('.del').each(function() {
    $(this).on('click', function() {
        return confirm('Are You Sure?');
    });
});

$('#checkAll').on('click', function() {
    $('.permission').each(function() {
        $(this).prop('checked', $('#checkAll').prop('checked'));
    });
});
</script>
@endsection
