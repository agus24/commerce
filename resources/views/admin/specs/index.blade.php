@extends('layouts.admin.app')

@section('content')
<br>
<div id="base">
<div id="content">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head"><header>Ukuran</header></div>
                    <div class="card-body">
                    @can('Add Specification')
                        <a href="{{ url('/admin-page/specs/create') }}" class="btn btn-success btn-sm" title="Add New spec">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    @endcan

                        {!! Form::open(['method' => 'GET', 'url' => '/admin-page/specs', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th width="5%">No.</th>
                                        <th>Name</th>
                                        <th width="25%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($specs as $key=>$item)
                                    <tr>
                                        <td>{{ $key+1 }}.</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                        @can('Edit Specification')
                                            <a href="{{ url('/admin-page/specs/' . $item->id . '/edit') }}" title="Edit spec"><button class="btn btn-primary btn-xs" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        @endcan
                                        @can('Delete Specification')
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin-page/specs', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete spec',
                                                        !Auth::user()->hasPermission('Delete Specification')? "disabled" : "",
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="pagination-wrapper"> {!! $specs->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
</div>
@endsection
