<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ Config::get('app.name') }}</title>

        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="your,keywords">
        <meta name="description" content="Short explanation about this website">
        <!-- END META -->

        <!-- BEGIN STYLESHEETS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/bootstrap.css?1422792965') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/materialadmin.css?1425466319') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/font-awesome.min.css?1422529194') }}" />
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/material-design-iconic-font.min.css?1421434286') }}" />
        <!-- END STYLESHEETS -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
        <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
        <![endif]-->
    </head>
    <body class="menubar-hoverable header-fixed ">
        <!-- END HEADER-->

        <!-- BEGIN BASE-->
        <div id="base">

            <!-- BEGIN OFFCANVAS LEFT -->
            <div class="offcanvas">
            </div><!--end .offcanvas-->
            <!-- END OFFCANVAS LEFT -->

            <!-- BEGIN CONTENT-->
            <div id="content">

                <!-- BEGIN 404 MESSAGE -->
                <section>
                    <div class="section-body contain-lg">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h1><span class="text-xxxl text-light">{{ $e->getMessage() }}</span></h1>
                                <h2 class="text-light"></h2>
                            </div><!--end .col -->
                        </div><!--end .row -->
                    </div><!--end .section-body -->
                </section>
                <!-- END 404 MESSAGE -->
            </div><!--end #content-->

        </div><!--end #base-->
        <!-- END BASE -->

    </body>
</html>
