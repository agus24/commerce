@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-12">
                <div data-layout="masonry" data-masonry-column="4" class="commerce products-masonry masonry">
                    <div class="masonry-filter">
                        <div class="filter-action filter-action-center">
                            <ul data-filter-key="filter">
                                @foreach($brands as $key => $brand)
                                <li><a data-masonry-toogle="selected" data-filter-value=".{{$brand->name}}" href="{{ $brand->name }}"> {{ $brand->name }} </a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="products-masonry-wrap">
                        <ul class="products masonry-products row masonry-wrap">
                        @foreach($products as $key_group => $product_group)
                            @foreach($product_group as $product)
                                <li class="product masonry-item product-no-border style-2 col-md-3 col-sm-6 {{ $key_group }} donec">
                                    <div class="product-container">
                                        <figure>
                                            <div class="product-wrap">
                                                <div class="product-images">
                                                    <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                        <a href="{{ url('/product/'.$product->name) }}"><img width="450" height="450" src="{{ asset('Storage/image/product/'.$product->picture) }}" alt=""/></a>
                                                    </div>
                                                    <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                        <p>{{ $product->description }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <figcaption>
                                                <div class="shop-loop-product-info">
                                                    <div class="info-meta clearfix">
                                                    </div>
                                                    <div class="info-content-wrap">
                                                        <h3 class="product_title">
                                                            <a href="shop-detail-1.html">{{ $product->name }}</a>
                                                        </h3>
                                                        <div class="info-price">
                                                            <span class="price">
                                                                <span class="amount">Rp. {{ number_format($product->price) }}</span>
                                                            </span>
                                                        </div>
                                                        <div class="loop-action">
                                                            <div class="loop-add-to-cart">
                                                                <a href="{{ url('addCart/'.$product->id) }}" class="add_to_cart_button">
                                                                    Add to cart
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </li>
                            @endforeach
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
