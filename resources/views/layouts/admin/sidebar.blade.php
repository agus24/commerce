<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="../../html/dashboards/dashboard.html">
                <span class="text-lg text-bold text-primary ">{{ Config::get('app.name') }}</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">
        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">
            <!-- BEGIN UI -->
            @if(Auth::user()->hasPermission('View Brands') || Auth::user()->hasPermission('View Specification') || Auth::user()->hasPermission('View Products'))
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="fa fa-cube fa-fw"></i></div>
                    <span class="title">Products</span>
                </a>
                <!--start submenu -->
                <ul>
                    @can('View Brands')
                    <li><a href="{{ url(Config::get('app.admin_url').'/brands') }}" ><span class="title">Brands</span></a></li>
                    @endcan
                    @can('View Specification')
                    <li><a href="{{ url(Config::get('app.admin_url').'/specs') }}" ><span class="title">Specification</span></a></li>
                    @endcan
                    @can('View Products')
                    <li><a href="{{ url(Config::get('app.admin_url').'/products') }}" ><span class="title">Products</span></a></li>
                    @endcan
                </ul><!--end /submenu -->
            </li>
            @endif

            @can('View Order')
            <li>
                <a href="{{ url(Config::get('app.admin_url').'/order') }}" >
                    <div class="gui-icon"><i class="md md-settings-ethernet"></i></div>
                    <span class="title">Order</span>
                </a>
            </li>
            @endcan

            @if(
                Auth::user()->hasPermission('View User') ||
                Auth::user()->hasPermission('View Company') ||
                Auth::user()->hasPermission('View Discount')
            )
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="md md-settings"></i></div>
                    <span class="title">Settings</span>
                </a>
                <!--start submenu -->
                <ul>
                @can('View User')
                    <li><a href="{{ url(Config::get('app.admin_url').'/user') }}" ><span class="title">User Manager</span></a></li>
                @endcan
                @can('View Company')
                    <li><a href="{{ url(Config::get('app.admin_url').'/company') }}" ><span class="title">Company</span></a></li>
                @endcan
                @can('View Discount')
                    <li><a href="{{ url(Config::get('app.admin_url').'/discount') }}" ><span class="title">Discount</span></a></li>
                @endcan
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            @endif

        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

        {{-- <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
                <span class="opacity-75">Copyright &copy; 2017</span> <strong>Gustiawan Ouwawi</strong>
            </small>
        </div> --}}
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
