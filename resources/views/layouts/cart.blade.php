<div class="minicart-side">
    <div class="minicart-side-title">
        <h4>Shopping Cart</h4>
    </div>
    <div class="minicart-side-content">
        <div class="minicart">
            <div class="minicart-header no-items show">
                <table class="table table-responsive">
                    <thead>
                        <th width="5%">No.</th>
                        <th width="">Item Name</th>
                        <th width="30%">Qty</th>
                        <th width="15%">#</th>
                    </thead>
                    <tbody>
                    @foreach($cart as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->product_name }}</td>
                        <td>
                            <a href="{{ url('modifyCart/plus/'.$value->product_id) }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                {{ $value->qty }}
                            <a href="{{ url('modifyCart/min/'.$value->product_id) }}" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i></a>
                        </td>
                        <td><a href="{{ url('removeCart/'.$value->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="minicart-footer">
                <div class="minicart-actions clearfix">
                    <a class="button no-item-button" href="{{ url("checkout") }}">
                        <span class="text">Checkout</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
