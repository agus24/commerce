@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-10 col-sm-offset-1">
                <ul>
                    <li><b><h4>Sejarah Umum Perusahaan</h4></b></li>
                    <li>
                        <p>PT.Duta Abadi Primantara berdiri pada 19 November tahun 1990,berdasarkan SIUP Pemerintah tentang perusahaan menengah No 3262/09-02/PM/XI/1990 dengan nama PT.Duta Abadi Primantara.</p>
                        <p>PT.Duta Abadi Primantara adalah salah satu perusahaan SpringBed yang terkenal dan terbesar di Indonesia yang memproduksi beberapa jenis springbed  KINGKOIL,SERTA,TEMPUR,FLORENCE juga BEDDING ACCESSORIES. Perkembangan yang sangat pesat, ini ditandai dengan dibukanya beberapa cabang perusahaansepertidiMedan,PekanBaru,Palembang,semarang,surabaya,bali,manado dan apalagi di tangerang sebagai pabrik utamanya jelas terjadi perkembangan yang sangat signifikan.</p>
                        <p>Kantor pusat perusahaan ini terletak di Jalan Sultan Agung No 16 Jakarta 12980. Pendiri perusahaan ini adalah Bapak Hendry Setiawan selaku Managing Director. Pabrik utama dari PT.Duta Abadi Primantara berdomisili di kota Tangerang tepatnya di Jalan Raya Mauk Km 2.1,Jln.Galeong No 7 Kelurahan Margasari  kecamatan karawaci Tangerang 15113 juga berkembang dengan sangat pesat baik dari segi kuantitas dan kualitas produksi,kuantitas gedung serta pertambahan buruhnya. Secara umum pabrik utama ini cukup memberi andil besar terhadap pertumbuhan ekonomi di daerah tangerang dan sekitarnya.</p>
                    </li>
                    <li><b><h4>Visi</h4></b></li>
                    <li><p>Menjadi perusahaan manufaktur produksi Springbed yang terpandang dan terpercaya melalui keuntungan inovasi, manjemen,dan kompetensi SDM.</p></li>
                    <li><b><h4>Misi</h4></b></li>
                    <li><p>Menciptakan   dan   menghasilkan   produk-produk   Springbed   yang menarik, baru, inovatif dan bermutu serta meningkatkan efisiensi dan produksi disemua proses bisnisnya.</p></li>
                    <li>&nbsp;</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
