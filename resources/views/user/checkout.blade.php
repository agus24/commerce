@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-12">
            @if($cart->count() > 0)
                <div class="row">
                    <div class="col-md-6">
                        <p><b>Date : </b>{{ Carbon\Carbon::now() }}</p>
                    </div>
                </div>
                <table class="table table-bordered table-stripped">
                <thead>
                    <th width="5%"><center>No.</center></th>
                    <th><center>Product Name</center></th>
                    <th width="10%"><center>Qty</center></th>
                    <th width="15%"><center>Price</center></th>
                    <th width="20%"><center>Total</center></th>
                    <th width="5%">#</th>
                </thead>
                <tbody>
                <?php $total = 0; ?>
                @foreach($cart as $key => $value)
                <tr>
                    <td>{{ $key+1 }}.</td>
                    <td>{{ $value->product_name }}</td>
                    <td align="right">{{ $value->qty }}</td>
                    <td align="right">{{ number_format($value->price) }}</td>
                    <td align="right">{{ number_format($value->qty * $value->price) }}</td>
                    <td><a href="{{ url('removeCart/'.$value->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                </tr>
                <?php $total += $value->qty * $value->price; ?>
                @endforeach
                <tr>
                    <td colspan="4" align="right"><b>Total</b></td>
                    <td align="right">{{ number_format($total) }}</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
                </table>

                <div class="col-md-3 col-md-offset-5">
                    <a href="{{ url('payment') }}" class="btn btn-primary">Procced to payment</a>
                </div>
            @else
                <center>
                    <h2> Your Cart Is Empty. </h2><br>
                    <a href="{{ url('/') }}" class="btn btn-success">Click here to start shopping.</a>
                </center>
            @endif
            </div>
        </div>
    </div>
@endsection
