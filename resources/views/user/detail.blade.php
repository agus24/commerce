@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row row-fluid mt-2">
        <div class="col-sm-12">
            <table class="table table-bordered table-stripped">
                <thead>
                    <th width="5%"><center>No.</center></th>
                    <th width=""><center>Product Name</center></th>
                    <th width="5%"><center>Qty</center></th>
                    <th width="15%"><center>Price</center></th>
                    <th width="15%"><center>Total</center></th>
                </thead>
                <tbody>
                @foreach($detail as $key => $value)
                <tr>
                    <td>{{ $key+1 }}.</td>
                    <td>{{ $value->name }}</td>
                    <td align="right">{{ $value->qty }}</td>
                    <td align="right">{{ number_format($value->price) }}</td>
                    <td align="right">{{ number_format($value->price * $value->qty) }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
