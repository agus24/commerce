@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-12">
                <div class="col-md-offset-2 col-md-8">
                    <form method="POST" action="{{ url('user/profile') }}">
                    {!! csrf_field() !!}
                        <ul class="list-group">
                            <li class="list-group-item">
                                Name
                                <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}">
                            </li>
                            <li class="list-group-item">
                                Address
                                <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}">
                            </li>
                            <li class="list-group-item">
                                City
                                <input type="text" name="city" class="form-control" value="{{ Auth::user()->city }}">
                            </li>
                            <li class="list-group-item">
                                Province
                                <input type="text" name="province" class="form-control" value="{{ Auth::user()->province }}">
                            </li>
                            <li class="list-group-item">
                                Phone
                                <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone_number }}">
                            </li>
                            <li class="list-group-item">
                                <center><button href="{{ url('user/profile/edit') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Save</button></center>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
