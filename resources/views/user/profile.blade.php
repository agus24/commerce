@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-12">
                <div class="col-md-offset-2 col-md-8">
                    <ul class="list-group">
                        <li class="list-group-item">
                            Name <span class="badge">{{ Auth::user()->name }}</span>
                        </li>
                        <li class="list-group-item">
                            Address <span class="badge">{{ Auth::user()->address }}</span>
                        </li>
                        <li class="list-group-item">
                            City <span class="badge">{{ Auth::user()->city }}</span>
                        </li>
                        <li class="list-group-item">
                            Province <span class="badge">{{ Auth::user()->province }}</span>
                        </li>
                        <li class="list-group-item">
                            Phone <span class="badge">{{ Auth::user()->phone_number }}</span>
                        </li>
                        <li class="list-group-item">
                            <center><a href="{{ url('user/profile/edit') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a></center>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
