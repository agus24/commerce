@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-10 col-sm-offset-1">
                <b><h4>Term And Condition</h4></b>
                <ul style="list-style: decimal !important">
                    <li>pembayaran yang sah hanya pada rekening BCA pt.duta abadi primantara</li>
                    <li>garansi perusahaan 6 tahun</li>
                    <li>orderan luar kota dikirim langsung dari cabang perusahaan terdekat</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
