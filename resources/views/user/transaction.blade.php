@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row row-fluid mt-2">
            <div class="col-sm-12">
                <table class="table table-bordered table-stripped">
                <thead>
                    <th width="5%"><center>No.</center></th>
                    <th width="20%"><center>Date</center></th>
                    <th width="15%"><center>Total</center></th>
                    <th width="15%"><center>Discount</center></th>
                    <th width="15%"><center>Grand Total</center></th>
                    <th width="15%"><center>Payment</center></th>
                    <th width="10%"><center>#</center></th>
                </thead>
                <tbody>
                @foreach($transactions as $key => $transaction)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $transaction->order_time }}</td>
                    <td align="right">Rp. {{ number_format($transaction->total_order) }}</td>
                    <td align="right">Rp. {{ number_format($transaction->discount) }}</td>
                    <td align="right">Rp. {{ number_format($transaction->total_order - $transaction->discount) }}</td>
                    @if(($transaction->total_order - $transaction->discount) == $transaction->total_payment)
                    <td align="right" style="background-color:#9adc9a">Rp. {{ number_format($transaction->total_payment) }}</td>
                    @else
                    <td align="right" style="background-color:#fd9d9d">Rp. {{ number_format($transaction->total_payment) }}</td>
                    @endif
                    <td><a href="{{ url('user/transaction/'.$transaction->id) }}" class="btn btn-success"><i class="fa fa-eye"></i> View Detail</a>
                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
