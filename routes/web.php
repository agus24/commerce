<?php

use App\UserCart;
Route::get('/', 'CommerceController@index');

Route::get('logout', function() {
    Auth::logout();
    return redirect('/');
});

Route::get('about-us',function() {
    return view('user.aboutus');
});

Route::get('termcondition',function() {
    return view('user.termcondition');
});

Route::get('term',function() {
    return 'under construction';
});

Route::group(['prefix' => 'user'], function() {
    Route::get('transaction', 'UserController@transaction');
    Route::get('transaction/{id}', 'UserController@transactionDetail');

    Route::get('profile','UserController@profile');
    Route::get('profile/edit','UserController@edit');
    Route::post('profile','UserController@update');
});

Route::post('/login', "LoginController@loginFront");
Route::post('/register', "LoginController@register");

Route::group(["prefix" => Config::get('app.admin_url')], function() {
    Route::get('/', "LoginController@index");
    Route::post('/login', "LoginController@loginAdmin");

    Route::group(['middleware' => 'admin'], function() {
        Route::get('/home', function() {
            return view('admin.home');
        });

        Route::get('logout', function() {
            Auth::logout();
            return redirect(Config::get('app.admin_url').'/');
        });

        Route::get('company', 'admin\\SettingController@company');
        Route::post('company', 'admin\\SettingController@companyUpdate');

        Route::get('discount', 'admin\\SettingController@discount');
        Route::post('discount', 'admin\\SettingController@discountUpdate');

        Route::get('user', 'admin\\SettingController@index');
        Route::get('user/{id}/edit', 'admin\\SettingController@edit');
        Route::patch('user/{id}', 'admin\\SettingController@update');
        Route::get('user/{id}/delete', 'admin\\SettingController@delete');
        Route::get('/user/{id}/permission',"admin\\SettingController@showPermission");
        Route::post('/user/{id}/permission',"admin\\SettingController@updatePermission");
        Route::get('/user/create', "admin\\SettingController@create");
        Route::post('/user', "admin\\SettingController@insert");


        Route::resource('/order', 'admin\\OrderController');
        Route::resource('brands', 'admin\\BrandsController');
        Route::resource('specs', 'admin\\SpecificationsController');
        Route::resource('products', 'admin\\ProductController');
        Route::get('/order/payment/{id}', 'admin\\OrderController@payment');
        Route::post('/order/payment/{id}', 'admin\\OrderController@addPayment');

        Route::group(["prefix" => "laporan"],function() {
            Route::get('penjualan', 'LaporanController@penjualanShow');
            Route::post('penjualan', 'LaporanController@penjualan');
            Route::get('user', 'LaporanController@userShow');
            Route::post('user', 'LaporanController@user');
        });
    });
});

Route::get('addCart/{product_id}', "CartController@addCart");
Route::get('removeCart/{id}', "CartController@removeCart");
Route::get('checkout', "CartController@checkout");
Route::get('payment', "CartController@order");
Route::get('modifyCart/{type}/{id}','CartController@modify');
